public class Cat
{
	public String breed;
	public String color;
	public int age;
	
	public void climbTree()
	{
		System.out.println("I just climbed this tree!");
	}
	
	public void knead()
	{
		if (age > 1)
		{
			System.out.println("I'm no kitten, I'd rather climb a tree than knead!");
		}
		
		else
		{
			System.out.println("I'm still just a kitten, so I just knead on fabric all day!");
		}
	}
}