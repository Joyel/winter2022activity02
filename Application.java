public class Application
{
	public static void main(String[] args)
	{
		Cat adultCat = new Cat();
		adultCat.breed = "Burmese";
		adultCat.color = "gray";
		adultCat.age = 5;
		
		System.out.println(adultCat.breed);
		System.out.println(adultCat.color);
		System.out.println(adultCat.age);
		
		Cat kitten = new Cat();
		kitten.breed = "Tabby";
		kitten.color = "brown";
		kitten.age = 1;
		
		System.out.println(kitten.breed);
		System.out.println(kitten.color);
		System.out.println(kitten.age);
		
		adultCat.knead();
		kitten.knead();
		
		Cat[] clowder = new Cat[3];
		clowder[0] = adultCat;
		clowder[1] = kitten;
		
		System.out.println(clowder[0].breed);
		
		clowder[2] = new Cat();
		clowder[2].breed = "Munchkin";
		clowder[2].color = "orange";
		clowder[2].age = 1;
		
		System.out.println(clowder[2].breed);
		System.out.println(clowder[2].color);
		System.out.println(clowder[2].age);
	}
}